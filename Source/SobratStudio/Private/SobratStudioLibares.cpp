// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.

#include "SobratStudio.h"
#include "../Classes/SobratStudioLibares.h"
#include "Online.h"
//#include "OnlineSubsystemSteam.h"
//#include "OnlineSessionInterfaceSteam.h"
//#include "OnlineSessionAsyncServerSteam.h"
//#include "GameFramework/GameStateBase.h"
//#include "GameFramework/PlayerState.h"
//#include "OnlineSubsystemUtils.h"
//#include "SocketSubsystem.h"
//#include "IPAddressSteam.h"
//#include "SteamSessionKeys.h"
//#include "SteamUtilities.h"
//#include "OnlineAuthInterfaceSteam.h"

int SteamTransactionPoweredNumber = -1;
int USobratStudioLibares::TransactionCallback()
{
	if (SteamTransactionPoweredNumber > -1)
	{
		int coobik = SteamTransactionPoweredNumber;
		SteamTransactionPoweredNumber = -1;
		return coobik;
	}
	else
	{
		return SteamTransactionPoweredNumber;
	}
		
}
//FString USobratStudioLibares::StructToJson(UProperty* AnyStruct)
//{
	//FString OutJsonString;
	//FJsonObjectConverter* Converter = new FJsonObjectConverter;
	//Converter->UStructToJsonObjectString(
	//	Struct,
	//	OutJsonString
	//return OutJsonString;
	//return "Hello";
//}
int64 USobratStudioLibares::StringToInt64(FString Number) {
	int64 PublishedFileID = FCString::Strtoui64(*Number, NULL, 10);
	return PublishedFileID;
}
FString USobratStudioLibares::Int64ToString(int64 Number) {
	FString returnString;
	//Conversion from uint64 to string!
	char temp[21];
	sprintf(temp, "%llu", Number);
	returnString = temp;
	return returnString;
}
void USobratStudioLibares::ActivateGameOverlayChat(const FString steamID)
{
	if (SteamAPI_Init()) {
		uint64 cID = FCString::Strtoui64(*steamID, NULL, 10);
		SteamFriends()->ActivateGameOverlayToUser("chat", cID);
	}
}
void USobratStudioLibares::GetServerID(FString & SteamId)
{
	//if (SteamAPI_Init()) {
		IOnlineSubsystem* ion = IOnlineSubsystem::Get();
		IOnlineSessionPtr isptr = ion->GetSessionInterface();
		FNamedOnlineSession* ses = isptr->GetNamedSession(GameSessionName);
		SteamId = ses->OwningUserId->ToString();
		return;
		
		//ISteamGameServer* SteamGameServerPtr = SteamGameServer();
		//check(SteamGameServerPtr);
		//IOnlineSessionPtr Session = IOnlineSubsystem::Get()->GetSessionInterface();
		//FUniqueNetIdSteam* SteamId = (FUniqueNetIdSteam*)(Sessions->OwningUserId.Get());
		//SteamId->OwningUserName;
		//USTRUCT* Two = Session->GetSessionSettings(GameSessionName);
	//}
	//else
	//{
	//	SteamId = "SteamAPI_Init() return false!";
	//	return;
	//}
}



void USobratStudioLibares::MicroTxnAuthorizationWarlock(MicroTxnAuthorizationResponse_t *pParam) {
	if (pParam->m_bAuthorized)
		SteamTransactionPoweredNumber = pParam->m_ulOrderID;
	else
		SteamTransactionPoweredNumber = 0;
	return;
}

void USobratStudioLibares::GameOverlayActivatedAA(GameOverlayActivated_t *pParam) {
	if (pParam->m_bActive) {
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Open");
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, "Close");
	}
	return;
}

void USobratStudioLibares::GameRichPresenceJoinRequested(GameRichPresenceJoinRequested_t *pParam) {
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, "InviteGameAccept");
	return;
}

bool USobratStudioLibares::SetOuter(UObject* Object, UObject* NewOuter) {
	return Object->Rename(*Object->GetName(), NewOuter);
}

/*void UCBS_Module::ExecFunction(UObject* Object, UFunction* Function, UProperty* Attributes) {
	if (Function == nullptr) { return; }

	FStructOnScope FuncParam(Function);
	UProperty* ReturnProp = nullptr;

	/*for (TFieldIterator<UProperty> It(Function); It; ++It)
	{
		UProperty* Prop = *It;
		if (Prop->HasAnyPropertyFlags(CPF_ReturnParm))
		{
			ReturnProp = Prop;
		}
		else
		{
			ReturnProp = Attributes;
			//FillParam here            
		}
	}

	Object->ProcessEvent(Function, Attributes);
	return;
}*/




//TEMPLATE Load Obj From Path
/*template <typename ObjClass>
UObject* USobratStudioLibares::GetObjectFromPath2(FString Path) {
	//uint8 I = P.Find("/", ESearchCase::CaseSensitive, ESearchDir::FromEnd) + 1;
	//FString O = "V" + P.Mid(I, P.Len() - I);
	if (Path == NAME_None) return NULL;
	return Cast<ObjClass>(StaticLoadObject(ObjClass::StaticClass(), NULL, *Path.ToString()));
}*/


//void USobratStudioLibares::PingResponse(ISteamMatchmakingPlayersResponse *pParam) {
//	IAdvancedFriendsInterface::Execute_OnSessionInviteAccepted(Player, PInvited, BluePrintResult);
//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, "PingSuccess!");
//	return;
//}