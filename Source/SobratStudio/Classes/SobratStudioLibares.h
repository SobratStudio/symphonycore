// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4996)
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC || PLATFORM_LINUX

#pragma push_macro("ARRAY_COUNT")
#undef ARRAY_COUNT

#include <steam/steam_api.h>

#pragma pop_macro("ARRAY_COUNT")

#endif
//#include "steam/steam_api.h"
//#include "steam/isteamuser.h"
#include "Engine.h"
//#include "OnlineSubsystemSteamPrivate.h"
//#include "OnlineAsyncTaskManager.h"
//#include "OnlineSubsystemSteamPackage.h"

//��� Json
//#include "../../SIOJson/Public/ISIOJson.h"


#include "SobratStudioLibares.generated.h"




/**
 * 
 */
UCLASS()
class USobratStudioLibares : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

private:
	    STEAM_CALLBACK(USobratStudioLibares, MicroTxnAuthorizationWarlock, MicroTxnAuthorizationResponse_t);
		STEAM_CALLBACK(USobratStudioLibares, GameOverlayActivatedAA, GameOverlayActivated_t);
		//STEAM_CALLBACK(USobratStudioLibares, GameLobbyJoinRequested, GameLobbyJoinRequested_t);
		STEAM_CALLBACK(USobratStudioLibares, GameRichPresenceJoinRequested, GameRichPresenceJoinRequested_t);
		
public:
		
		UFUNCTION(BlueprintCallable, Category = "Sobrat")
		static int TransactionCallback();

		//UFUNCTION(BlueprintCallable, Category = "Sobrat", CustomThunk, meta = (CustomStructureParam = "AnyStruct"))
		//static FString StructToJson(UProperty* AnyStruct);
		
		UFUNCTION(BlueprintPure, meta = (BlueprintAutocast), Category = "Sobrat")
		static int64 StringToInt64(FString Number);

		UFUNCTION(BlueprintPure, meta = (BlueprintAutocast), Category = "Sobrat")
		static FString Int64ToString(int64 Number);

		UFUNCTION(BlueprintCallable, Category = "Sobrat")
		static bool SetOuter(UObject* Object, UObject* NewOuter);

		UFUNCTION(BlueprintCallable, Category = "Sobrat|Steam")
		static void ActivateGameOverlayChat(const FString steamID);

		UFUNCTION(BlueprintPure, Category = "Sobrat|Steam")
		static void GetServerID(FString & SteamId);

		/*UFUNCTION(BlueprintPure, meta = (ToolTip = "{Object}'/Game/+GetDefault<UCstGmStts>()->Ptc+{Pat�h}/{ObjectName}.{ObjectName}'", BlueprintAutocast), Category = "Conversion")
		static UObject* GetObjectFromPath2(FString Path);*/
		
};