// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.
using UnrealBuildTool;
using System.IO;

public class SobratStudio : ModuleRules
	{
		public SobratStudio(ReadOnlyTargetRules Target) : base (Target)
		{


			PrivateIncludePaths.AddRange(
				new string[] {
					"SobratStudio/Private"
				}
				);
        //Definitions.Add("WITH_ADVANCED_STEAM_SESSIONS=1");

        
		PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"OnlineSubsystem", 
					"OnlineSubsystemUtils",
                    "Json",
                    "JsonUtilities"
					//"AdvancedSessions"
				}
				);
				PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem", "Sockets", "Networking", "OnlineSubsystemUtils"});
		if ((Target.Platform == UnrealTargetPlatform.Win64) || (Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Linux) || (Target.Platform == UnrealTargetPlatform.Mac))
        {
            PublicDependencyModuleNames.AddRange(new string[] { "Steamworks", "OnlineSubsystemSteam" });
            PublicIncludePaths.AddRange(new string[] { "../Plugins/Online/OnlineSubsystemSteam/Source/Private" });// This is dumb but it isn't very open
        }

		}
	}
