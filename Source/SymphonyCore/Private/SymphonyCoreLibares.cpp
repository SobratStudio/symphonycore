// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.

#include "SymphonyCore.h"
#include "../Classes/SymphonyCoreLibares.h"
#include "Online.h"

TArray<FString> USymphonyCoreLibares::GetDirectoryNames(const FString& Directory)
{
	class FLowEntryFileManagerGetDirectoriesVisitor : public IPlatformFile::FDirectoryVisitor
	{
	public:
		TArray<FString> Directories;
		FLowEntryFileManagerGetDirectoriesVisitor()
		{
		}
		virtual bool Visit(const TCHAR* FileName, bool bIsDirectory)
		{
			if (bIsDirectory)
			{
				FString Path;
				FString Name;
				USymphonyCoreLibares::SplitPath(FileName, Path, Name);
				Directories.Add(Name);
			}
			return true;
		}
	};
	FLowEntryFileManagerGetDirectoriesVisitor GetFoldersVisitor;
	FPlatformFileManager::Get().GetPlatformFile().IterateDirectory(*Directory, GetFoldersVisitor);
	return GetFoldersVisitor.Directories;
}

TArray<FString> USymphonyCoreLibares::GetDirectories(const FString& Directory)
{
	class FLowEntryFileManagerGetDirectoriesVisitor : public IPlatformFile::FDirectoryVisitor
	{
	public:
		TArray<FString> Directories;
		FLowEntryFileManagerGetDirectoriesVisitor()
		{
		}
		virtual bool Visit(const TCHAR* FileName, bool bIsDirectory)
		{
			if (bIsDirectory)
			{
				Directories.Add(FileName);
			}
			return true;
		}
	};
	FLowEntryFileManagerGetDirectoriesVisitor GetFoldersVisitor;
	FPlatformFileManager::Get().GetPlatformFile().IterateDirectory(*Directory, GetFoldersVisitor);
	return GetFoldersVisitor.Directories;
}

void USymphonyCoreLibares::SplitPath(const FString& Path, FString& PathPart, FString& NamePart)
{
	FString LocalPath = Path;
	FPaths::NormalizeDirectoryName(LocalPath);
	if (LocalPath.Len() <= 0)
	{
		PathPart = TEXT("");
		NamePart = TEXT("");
	}
	bool Split = LocalPath.Split(TEXT("/"), &PathPart, &NamePart, ESearchCase::CaseSensitive, ESearchDir::Type::FromEnd);
	if (!Split)
	{
		PathPart = TEXT("");
		NamePart = LocalPath;
	}
	if (Path.Len() > 0)
	{
		PathPart += TEXT("/");
	}
}

void USymphonyCoreLibares::GetClassWithName(const FString& ClassName, UClass*& Class_, bool& Success)
{
	Class_ = NULL;
	Success = false;

	{// get class >>
		UClass* FoundClass = FindObject<UClass>(ANY_PACKAGE, *ClassName);
		if (FoundClass != nullptr)
		{
			Class_ = FoundClass;
			Success = true;
			return;
		}
	}// get class <<

	{// get class through redirector >>
		UObjectRedirector* RenamedClassRedirector = FindObject<UObjectRedirector>(ANY_PACKAGE, *ClassName);
		if ((RenamedClassRedirector != nullptr) && (RenamedClassRedirector->DestinationObject != nullptr))
		{
			UClass* FoundClass = CastChecked<UClass>(RenamedClassRedirector->DestinationObject);
			if (FoundClass != nullptr)
			{
				Class_ = FoundClass;
				Success = true;
				return;
			}
		}
	}// get class through redirector <<
}