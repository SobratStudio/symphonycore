// Fill out your copyright notice in the Description page of Project Settings.

#include "SymphonyCore.h"
#include "SC_Module.h"

/*void USC_Module::ExecuteFunctionByName(FString Function, FString Arguments) {
	FOutputDeviceNull ar;
	const FString Cmd = Function + ' ' + Arguments;
	this->CallFunctionByNameWithArguments(*Cmd, ar, NULL, true);
	return;
}*/

UFunction* USC_Module::FindFunctionBlueprint(FName Function) {
	return this->FindFunction(Function);
}

bool USC_Module::DelegateWitchoutArguments(UFunction* Function) {
	if (Function == nullptr) { return false; }
	this->ProcessEvent(Function, nullptr);
	return true;
}
