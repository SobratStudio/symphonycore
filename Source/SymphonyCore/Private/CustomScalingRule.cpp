// Fill out your copyright notice in the Description page of Project Settings.

#include "SymphonyCore.h"
#include "CustomScalingRule.h"

float ScaleDpi = 1.0f;


float UCustomScalingRule::GetDPIScaleBasedOnSize(FIntPoint Size) const
{
	float Scale = static_cast<float>(Size.GetMin()) * 0.001f * ScaleDpi;
	return Scale;
}

void UCustomScalingRule::SetDPIScaleBasedOnSize(float Scale)
{
	ScaleDpi = Scale;
}