// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.
using UnrealBuildTool;
using System.IO;

public class SymphonyCore : ModuleRules
	{
		public SymphonyCore(ReadOnlyTargetRules Target) : base (Target)
		{
			PrivateIncludePaths.AddRange(
				new string[] {
					"SymphonyCore/Private"
				}
				);
        
		PublicDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"CoreUObject",
					"Engine",
					"OnlineSubsystem", 
					"OnlineSubsystemUtils",
                    "Json",
                    "JsonUtilities"
				}
				);
				PrivateDependencyModuleNames.AddRange(new string[] { "OnlineSubsystem", "Sockets", "Networking", "OnlineSubsystemUtils"});
		}
	}
