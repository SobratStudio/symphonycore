// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameFramework/SaveGame.h"
#include "Engine.h"

#include "SC_Module.generated.h"

/**
 * 
 */
UCLASS()
class SYMPHONYCORE_API USC_Module : public USaveGame
{
	GENERATED_BODY()
	
	public:

	/*UFUNCTION(BlueprintCallable, Category = "Sobrat|SymphonyCore")
	void ExecuteFunctionByName(FString Function, FString Arguments);*/

	UFUNCTION(BlueprintCallable, Category = "Sobrat|SymphonyCore")
	UFunction* FindFunctionBlueprint(FName Function);

	UFUNCTION(BlueprintCallable, Category = "Sobrat|SymphonyCore")
	bool DelegateWitchoutArguments(UFunction* Function);

	UFUNCTION(BlueprintCallable, CustomThunk, meta = (AutoCreateRefTerm = "Module, Attributes",CustomStructureParam = "Attributes"), Category = "Sobrat|SymphonyCore")
	bool Delegate(USC_Module* Module, UFunction* Function, UProperty* Attributes);

	//Convert property into c++ accessible form
	DECLARE_FUNCTION(execDelegate)
	{
		P_GET_OBJECT(USC_Module, Module);
		P_GET_OBJECT(UFunction, Function);

		//Get properties and pointers from stack
		Stack.Step(Stack.Object, NULL);
		UStructProperty* StructProperty = ExactCast<UStructProperty>(Stack.MostRecentProperty);
		void* StructPtr = Stack.MostRecentPropertyAddress;


		// We need this to wrap up the stack
		P_FINISH;

		if (Module == nullptr) { *(bool*)RESULT_PARAM = false;  return; }
		if (Function == nullptr) { *(bool*)RESULT_PARAM = false;  return; }
		Module->ProcessEvent(Function, StructPtr);
		*(bool*)RESULT_PARAM = true;
		
	}
};
