// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DPICustomScalingRule.h"
#include "CustomScalingRule.generated.h"

/**
 * �������� ��������������� ��������. ����� �������� � Project Settings
 */
UCLASS()
class UCustomScalingRule : public UDPICustomScalingRule
{
	GENERATED_BODY()
public:
	virtual float GetDPIScaleBasedOnSize(FIntPoint Size) const;

	UFUNCTION(BlueprintCallable)
	static void SetDPIScaleBasedOnSize(float Scale);
};