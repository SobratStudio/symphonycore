// Copyright 2016-2019 Sobrat Studio. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"

#include "Engine.h"


#include "SymphonyCoreLibares.generated.h"




/**
 * 
 */
UCLASS()
class USymphonyCoreLibares : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
		
public: 

	UFUNCTION(BlueprintPure, Category = "Sobrat|SymphonyCore|Directory", Meta = (DisplayName = "Get Directory Names"))
	static TArray<FString> GetDirectoryNames(const FString& Directory);

	UFUNCTION(BlueprintPure, Category = "Sobrat|SymphonyCore|Directory", Meta = (DisplayName = "Get Directories"))
	static TArray<FString> GetDirectories(const FString& Directory);

	UFUNCTION(BlueprintPure, Category = "Sobrat|SymphonyCore", Meta = (DisplayName = "Split Path"))
	static void SplitPath(const FString& Path, FString& PathPart, FString& NamePart);

	/**
	* Finds a class.
	*
	* The given ClassName has to be a specific path to your class/object, for example: /Game/Blueprints/GameMode.GameMode_C
	*
	* Note that you have to append _C to the end of your ClassName, as is done in the example.
	* Note that is also always has to start with /Game/, no matter what the path to your class is.
	*
	* The easiest way to get the correct ClassName is by right clicking your class/object in the content browser and by then clicking on 'Copy Reference', then you'll just have to remove the 'Blueprint' and the quotes and add the _C at the end of it.
	* So this: Blueprint'/Game/Blueprints/PlayerController.PlayerController'
	* Would turn into this: /Game/Blueprints/PlayerController.PlayerController_C
	*/
	UFUNCTION(BlueprintCallable, Category = "Sobrat|SymphonyCore|Utilities|Other", Meta = (DisplayName = "Get Class With Name", Keywords = "get retrieve create object path find by value"))
	static void GetClassWithName(const FString& ClassName, UClass*& Class_, bool& Success);
};
